#------------------------------------------------------------
#        Script MySQL.
#------------------------------------------------------------


#------------------------------------------------------------
# Table: formulaire
#------------------------------------------------------------

CREATE TABLE formulaire(
        id_formulaire          Int  Auto_increment  NOT NULL ,
        formulaire_nom         Varchar (150) NOT NULL ,
        formulaire_prenom      Varchar (150) NOT NULL ,
        formulaire_mail        Varchar (150) NOT NULL ,
        formulaire_tel         Varchar (10) NOT NULL ,
        formulaire_description Text NOT NULL
	,PRIMARY KEY (id_formulaire)
)ENGINE=MyISAM;


#------------------------------------------------------------
# Table: admin
#------------------------------------------------------------

CREATE TABLE admin(
        id_admin     Int  Auto_increment  NOT NULL ,
        admin_mail   Varchar (100) NOT NULL ,
        admin_mdp    Varchar (100) NOT NULL ,
        admin_nom    Varchar (100) NOT NULL ,
        admin_prenom Varchar (100) NOT NULL
	, PRIMARY KEY (id_admin)
)ENGINE=MyISAM;


#------------------------------------------------------------
# Table: race
#------------------------------------------------------------

CREATE TABLE race(
        id_race Int  Auto_increment  NOT NULL ,
        race_type  Varchar (100) NOT NULL
	,PRIMARY KEY (id_race)
)ENGINE=MyISAM;


#------------------------------------------------------------
# Table: adresse
#------------------------------------------------------------

CREATE TABLE adresse(
        id_adresse          Int  Auto_increment  NOT NULL ,
        adresse_ville       Varchar (150) NOT NULL ,
        adresse_code_postal Varchar (5) NOT NULL
	,PRIMARY KEY (id_adresse)
)ENGINE=MyISAM;

#------------------------------------------------------------
# Table: genre
#------------------------------------------------------------

CREATE TABLE genre(
        id_genre          Int  Auto_increment  NOT NULL ,
        genre_type        Varchar (50) NOT NULL
        ,PRIMARY KEY (id_genre)    
)ENGINE=MyISAM;

#------------------------------------------------------------
# Table: annonce
#------------------------------------------------------------

CREATE TABLE annonce(
        id_annonce          Int  Auto_increment  NOT NULL ,
        annonce_nom         Varchar (100) NOT NULL ,
        annonce_age         Date NOT NULL ,
        annonce_photo       Text NOT NULL ,
        annonce_poids       Int NOT NULL ,
        annonce_taille      Int NOT NULL ,
        annonce_auteur      Varchar (150) NOT NULL ,
        annonce_caractere   Text NOT NULL ,
        annonce_description Text NOT NULL ,
        annonce_tel         Varchar (10) NOT NULL ,
        annonce_mail        Varchar (150) NOT NULL ,
        annonce_date        Date NOT NULL ,
        annonce_admin       Int NOT NULL ,
        annonce_race        Int NOT NULL ,
        annonce_adresse     Int NOT NULL,
        annonce_genre       Int NOT NULL
	, PRIMARY KEY (id_annonce)
)ENGINE=MyISAM;

