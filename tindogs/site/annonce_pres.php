<?php 
	include '../include/header.php';
	$post = $_GET['post'];
	include '../include/config.inc.php';
	$sql = 'SELECT * FROM annonce,race,admin,adresse,genre WHERE id_race=annonce_race AND id_admin=annonce_admin AND id_adresse=annonce_adresse AND id_genre=annonce_genre AND id_annonce=' . $post;
	$query = mysqli_query($lien, $sql);

	$article = (mysqli_fetch_assoc($query));
	$date = $article['annonce_age'];

	function age($date) {
		$age = date('Y') - date('Y', strtotime($date));
		if (date('md') < date('md', strtotime($date))) {
			return $age;
		}
		return $age;
	}

	echo "<div class='articleSection__container'>\n";
		include '../include/patoune_bg.php';
		echo "<div class='articleSection__item'>\n
				<div class='article__container'>\n
					<img class='annonce__img' src='" . $article['annonce_photo'] . "' alt='Une photo de notre magnifique chien'>\n
					<p class='annonce__nom'>" . $article['annonce_nom'] . "</p>\n
					<p class='annonce__race'>" . $article['annonce_description'] . "</p>\n
					<div class='annonceDescription__container'>\n
						<div class='annonceDescription__item'>\n
							<p class='annonceDescription__title'>À propos de " . $article['annonce_nom'] . "</p>\n
							<p class='annonceDescription__description'><span class='annonceDescription__subtitle'>Sexe : </span>" . $article['genre_type'] . "</p>\n
							<p class='annonceDescription__description'><span class='annonceDescription__subtitle'>Âge : </span>" . age($date) . " an(s)</p>\n
							<p class='annonceDescription__description'><span class='annonceDescription__subtitle'>Poids : </span>" . $article['annonce_poids'] . " kg</p>\n
							<p class='annonceDescription__description'><span class='annonceDescription__subtitle'>Taille : </span>" . $article['annonce_taille'] . " cm</p>\n
							<p class='annonceDescription__description'><span class='annonceDescription__subtitle'>Caractère : </span>" . $article['annonce_caractere'] . "</p>\n
						</div>\n
						<div class='annonceDescription__item'>\n
							<p class='annonceDescription__title'>À propos de son maître</p>\n
							<p class='annonceDescription__description'><span class='annonceDescription__subtitle'>Nom : </span>" . $article['annonce_auteur'] . "</p>\n
							<p class='annonceDescription__description'><span class='annonceDescription__subtitle'>Ville : </span>" . $article['adresse_ville'] . ", " . $article['adresse_code_postal'] . "</p>\n
							<p class='annonceDescription__description'><span class='annonceDescription__subtitle'>Tél : </span>" . $article['annonce_tel'] . "</p>\n
							<p class='annonceDescription__description'><span class='annonceDescription__subtitle'>Mail : </span>" . $article['annonce_mail'] . "</p>\n
						</div>\n
					</div>\n
				</div>\n
			</div>\n";
			include '../include/patoune_bg.php';
	echo "</div>";
?>