<?php 
	include "../include/header_admin.php";
	include "../include/config.inc.php";
	include "../include/access.php";
	$post = $_GET['post'];
	include '../include/config.inc.php';
	$sql = 'SELECT * FROM annonce,race,admin,adresse,genre WHERE id_annonce=' . $post;
	$query = mysqli_query($lien, $sql);

	$article = (mysqli_fetch_assoc($query));
echo "<div class='addSection__container'>\n";
include '../include/patoune_bg.php';
if(empty($_SESSION["admin"])) {
	echo "<p class='error_msg'>Veuillez vous connecter!</p>\n";
} else {
	echo "<div class='add__container'>\n
			<form class='add__form' enctype='multipart/form-data' action='#' method='post'>\n";
				echo update();
				echo "<p class='add__title'>Modifier une annonce - Faire attention, le formulaire est sensible à la casse. N'hésitez pas à contacter un admin en cas de soucis.</p>\n
				<label class='add__label' for='nom'>Nom de l'animal</label>\n
				<input class='add__input' type='text' name='nom' id='nom' value='" . $article['annonce_nom'] . "' required/>\n
				<label class='add__label' for='age'>Date de naissance de l'animal</label>\n
				<input class='add__input' type='date' name='age' id='age' value='" . $article['annonce_age'] . "' required/>\n
				<label class='add__label' for='photo'>Photo de l'animal (lien)</label>\n
				<input class='add__input' type='text' name='photo' id='photo' value='" . $article['annonce_photo'] . "' required/>\n
				<div class='addItem__container'>\n
					<div class='item'>\n
						<label class='add__label' for='poids'>Poids</label>\n
						<input class='add__input add__input--s' type='number' name='poids' id='poids' value='" . $article['annonce_poids'] . "' required>\n
					</div>\n
					<div class='item'>\n
						<label class='add__label' for='taille'>Taille</label>\n
						<input class='add__input add__input--s' type='number' name='taille' id='taille' value='" . $article['annonce_taille'] . "' required>\n
					</div>\n
				</div>\n
				<label class='add__label' for='maitre'>Nom du maître</label>\n
				<input class='add__input' type='text' name='maitre' id='maitre' value='" . $article['annonce_auteur'] . "' required/>\n
				<label class='add__label' for='tel'>Téléphone</label>\n
				<input class='add__input' type='number' name='tel' id='tel' value='" . $article['annonce_tel'] . "' required/>\n
				<label class='add__label' for='mail'>E-mail</label>\n
				<input class='add__input' type='mail' name='mail' id='mail' value='" . $article['annonce_mail'] . "' required/>\n
				<label class='add__label' for='caractere'>Caractère du chien</label>\n
				<textarea class='add__input add__input--msg' type='text' name='caractere' id='caractere' required>" . $article['annonce_caractere'] . "</textarea>\n
				<label class='add__label' for='description'>Description de l'annonce</label>\n
				<textarea class='add__input add__input--msg' type='text' name='description' id='description' required>" . $article['annonce_description'] . "</textarea>\n
				<label class='add__label' for='admin'>Administrateur</label>\n
				<input class='add__input' type='number' name='admin' id='admin' value='" . $article['annonce_admin'] . "' required/>\n
				<label class='add__label' for='race'>Race</label>\n
				<input class='add__input' type='number' name='race' id='race' value='" . $article['annonce_race'] . "' required/>\n
				<label class='add__label' for='adresse'>Adresse</label>\n
				<input class='add__input' type='number' name='adresse' id='adresse' value='" . $article['annonce_adresse'] . "' required/>\n
				<label class='add__label' for='genre'>Genre</label>\n
				<input class='add__input' type='number' name='genre' id='genre' value='" . $article['annonce_genre'] . "' required/>\n
				<input class='add__button' type='submit' name='Ajouter'>\n
			</form>\n
		</div>\n";

		include '../include/patoune_bg.php';
	echo "</div>";
}

function update() {
		include "../include/config.inc.php";
		if(!empty($_POST)) {
			$day = strtotime($_POST["age"]);
			$day = date('Y-m-d', $day);
			$sql_update = 'UPDATE annonce SET annonce_nom="' . $_POST['nom'] . '", annonce_age="' . $day . '", annonce_photo="' . $_POST['photo'] . '", annonce_poids="' . $_POST['poids'] . '", annonce_taille="' . $_POST['taille'] . '", annonce_auteur="' . $_POST['maitre'] . '", annonce_caractere="' . $_POST['caractere'] . '", annonce_description="' . $_POST['description'] . '", annonce_tel="' . $_POST['tel'] . '", annonce_mail="' . $_POST['mail'] . '", annonce_date="' . date('Y-m-d H:i:s') . '", annonce_admin="' . $_POST['admin'] . '", annonce_race="' . $_POST['race'] . '", annonce_adresse="' . $_POST['adresse'] . '", annonce_genre="' . $_POST['genre'] . '" WHERE id_annonce="' . $_GET['post'] . '"';
			if(mysqli_query($lien, $sql_update)) {
					return "<p class='form__confirm'>Bien réussi !!</p>\n";
			} else {
					return "<p class='form__confirm'>Quelque chose ne va pas... Veuillez réessayer</p>\n";
			}
		}
	}

?>