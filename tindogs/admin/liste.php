<?php include "../include/header_admin.php"; ?>
<?php 
include "../include/config.inc.php";
include "../include/access.php";

echo "<div class='listeSection__container'>\n";
include '../include/patoune_bg.php';
if(empty($_SESSION["admin"])) {
	echo "<p class='error_msg'>Veuillez vous connecter!</p>\n";
} else {
	$sql = 'SELECT * FROM annonce,race,admin,adresse WHERE id_race=annonce_race AND id_admin=annonce_admin AND id_adresse=annonce_adresse';
	$query = mysqli_query($lien, $sql);

	echo "<div class='table__container'>\n
			<table class='liste__container'>\n
				<tr class='liste__item liste__item--header'>\n
					<th class='liste__title'>Nom chien</th>\n
					<th class='liste__title'>Auteur</th>\n
					<th class='liste__title'>Admin</th>\n
					<th class='liste__title'>Date</th>
					<th>&nbsp;</th>
					<th>&nbsp;</th>\n
				</tr>\n";
	while($annonce = mysqli_fetch_assoc($query)) {
		echo "<tr class='liste__item'>\n
				<td class='liste__txt'>" . $annonce['annonce_nom'] . "</td>\n
				<td class='liste__txt'>" . $annonce['annonce_auteur'] . "</td>\n 
				<td class='liste__txt'>" . $annonce['admin_prenom'] . " " . $annonce['admin_nom'] . "</td>\n
				<td class='liste__txt'>" . $annonce['annonce_date'] . "</td>\n 
				<td class='liste__link'><a class='link white' href='./annonce_info.php?post=". $annonce['id_annonce'] . "'>Modifier</a></td>\n
				<td class='input__placement'><form action='liste.php' method='post'>\n
					<input class='liste__input' value='X' onclick='window.location.reload();' type='submit' name='" . $annonce['id_annonce'] . "'>\n
				</form></td>\n
			</tr>\n";

		if(!empty($_POST[$annonce["id_annonce"]])) {
			$sql_delete = 'DELETE FROM annonce WHERE id_annonce="' . $annonce["id_annonce"] . '"';
			mysqli_query($lien, $sql_delete);
			echo "<p class='form__confirm'>Bien supprimé !</p>";
		}
	}
	echo "</table>\n
	</div>\n";
}

if(!empty($_POST['deconnect'])) {
	unset($_SESSION['admin']);
	unset($_POST);
	header("location:index.php");
}
include "../include/patoune_bg.php";
echo "</div>\n";
?>