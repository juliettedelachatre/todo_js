<?php include "../include/header_admin.php"; ?>
<?php 
include "../include/config.inc.php";
include "../include/access.php";
echo "<div class='addSection__container'>\n";
include '../include/patoune_bg.php';
if(empty($_SESSION["admin"])) {
	echo "<p class='error_msg'>Veuillez vous connecter!</p>\n";
} else {
	echo "<div class='add__container'>\n
			<form class='add__form' enctype='multipart/form-data' action='#' method='post'>\n";
				echo add();
				echo "<p class='add__title'>Ajouter une annonce - Faire attention, le formulaire est sensible à la casse. N'hésitez pas à contacter un admin en cas de soucis.</p>\n
				<label class='add__label' for='nom'>Nom de l'animal</label>\n
				<input class='add__input' type='text' name='nom' id='nom' required/>\n
				<label class='add__label' for='age'>Date de naissance de l'animal</label>\n
				<input class='add__input' type='date' name='age' id='age' required/>\n
				<label class='add__label' for='photo'>Photo de l'animal (lien)</label>\n
				<input class='add__input' type='text' name='photo' id='photo' required/>\n
				<div class='addItem__container'>\n
					<div class='item'>\n
						<label class='add__label' for='poids'>Poids</label>\n
						<input class='add__input add__input--s' type='number' name='poids' id='poids' required>\n
					</div>\n
					<div class='item'>\n
						<label class='add__label' for='taille'>Taille</label>\n
						<input class='add__input add__input--s' type='number' name='taille' id='taille' required>\n
					</div>\n
				</div>\n
				<label class='add__label' for='maitre'>Nom du maître</label>\n
				<input class='add__input' type='text' name='maitre' id='maitre' required/>\n
				<label class='add__label' for='tel'>Téléphone</label>\n
				<input class='add__input' type='number' name='tel' id='tel' required/>\n
				<label class='add__label' for='mail'>E-mail</label>\n
				<input class='add__input' type='mail' name='mail' id='mail' required/>\n
				<label class='add__label' for='caractere'>Caractère du chien</label>\n
				<textarea class='add__input add__input--msg' type='text' name='caractere' id='caractere' required></textarea>\n
				<label class='add__label' for='description'>Description de l'annonce</label>\n
				<textarea class='add__input add__input--msg' type='text' name='description' id='description' required></textarea>\n
				<label class='add__label' for='admin'>Administrateur</label>\n
				<input class='add__input' type='number' name='admin' id='admin' required/>\n
				<label class='add__label' for='race'>Race</label>\n
				<input class='add__input' type='number' name='race' id='race' required/>\n
				<label class='add__label' for='adresse'>Adresse</label>\n
				<input class='add__input' type='number' name='adresse' id='adresse' required/>\n
				<label class='add__label' for='genre'>Genre</label>\n
				<input class='add__input' type='number' name='genre' id='genre' required/>\n
				<input class='add__button' type='submit' name='Ajouter'>\n
			</form>\n
		</div>\n";

}
include '../include/patoune_bg.php';
	echo "</div>";

	function add() {
		include "../include/config.inc.php";
		if(!empty($_POST)) {
			$day = strtotime($_POST["age"]);
			$day = date('Y-m-d', $day);
			$sql_add = "INSERT INTO annonce (annonce_nom, annonce_age, annonce_photo, annonce_poids, annonce_taille, annonce_auteur, annonce_caractere, annonce_description, annonce_tel, annonce_mail, annonce_date, annonce_admin, annonce_race, annonce_adresse, annonce_genre) VALUES ('" . $_POST['nom'] . "', '" . $day . "', '" . $_POST['photo'] . "', '" . $_POST['poids'] . "', '" . $_POST['taille'] . "', '" . $_POST['maitre'] . "', '" . $_POST['caractere'] . "', '" . $_POST['description'] . "', '" . $_POST['tel'] . "', '" . $_POST['mail'] . "', '" . date('Y-m-d') . "', '" . $_POST['admin'] . "', '" . $_POST['race'] . "', '" . $_POST['adresse'] . "', '" . $_POST['genre'] . "')";
			if(mysqli_query($lien, $sql_add)) {
				return "<p class='form__confirm'>Bien réussi !!</p>\n";
			} else {
				return "<p class='form__confirm'>Quelque chose ne va pas... Veuillez réessayer</p>\n";
			}
		}
	}

?>