<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="../styles/style.css">
  <link rel="icon" type="image/png" href="../assets/images/patte.png" />
  <title>Tindogs</title>
</head>
<body>
	<header class="header__background">
		<img class="header__logo" src="../assets/images/tindogs.png" alt="Notre logo">
		<div class="menu__container">
			<a href="./liste.php" class="menu__link"><p class="menu__item">Liste</p></a>
			<a href="./add.php" class="menu__link"><p class="menu__item">Ajouter</p></a>

			<?php 
				session_start();
				if(empty($_SESSION["admin"])) {
					echo "<a href='./index.php' class='menu__link'><p class='menu__item'>Se connecter</p></a>\n";
				} else {
					echo "<form action='liste.php' method='post'>\n
							<input class='menu__link menu__item menu__link--input' name='deconnect' type='submit' value='Se déconnecter'>\n
						</form>\n";
				}
			?>
		</div>
		<div class="header__line"></div>
	</header>