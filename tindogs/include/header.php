<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="../styles/style.css">
  <link rel="icon" type="image/png" href="../assets/images/patte.png" />
  <title>Tindogs</title>
</head>
<body>
	<header class="header__background">
		<img class="header__logo" src="../assets/images/tindogs.png" alt="Notre logo">
		<div class="menu__container">
			<a href="./index.php" class="menu__link"><p class="menu__item">Accueil</p></a>
			<a href="./annonce.php" class="menu__link"><p class="menu__item">Annonces</p></a>
			<a href="./contact.php" class="menu__link"><p class="menu__item">Contact</p></a>
		</div>
		<div class="header__line"></div>
	</header>