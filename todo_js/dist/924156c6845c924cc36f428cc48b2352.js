// modules are defined as an array
// [ module function, map of requires ]
//
// map of requires is short require name -> numeric require
//
// anything defined in a previous bundle is accessed via the
// orig method which is the require for previous bundles

// eslint-disable-next-line no-global-assign
require = (function (modules, cache, entry) {
  // Save the require from previous bundle to this closure if any
  var previousRequire = typeof require === "function" && require;

  function newRequire(name, jumped) {
    if (!cache[name]) {
      if (!modules[name]) {
        // if we cannot find the module within our internal map or
        // cache jump to the current global require ie. the last bundle
        // that was added to the page.
        var currentRequire = typeof require === "function" && require;
        if (!jumped && currentRequire) {
          return currentRequire(name, true);
        }

        // If there are other bundles on this page the require from the
        // previous one is saved to 'previousRequire'. Repeat this as
        // many times as there are bundles until the module is found or
        // we exhaust the require chain.
        if (previousRequire) {
          return previousRequire(name, true);
        }

        var err = new Error('Cannot find module \'' + name + '\'');
        err.code = 'MODULE_NOT_FOUND';
        throw err;
      }

      localRequire.resolve = resolve;

      var module = cache[name] = new newRequire.Module(name);

      modules[name][0].call(module.exports, localRequire, module, module.exports);
    }

    return cache[name].exports;

    function localRequire(x){
      return newRequire(localRequire.resolve(x));
    }

    function resolve(x){
      return modules[name][1][x] || x;
    }
  }

  function Module(moduleName) {
    this.id = moduleName;
    this.bundle = newRequire;
    this.exports = {};
  }

  newRequire.isParcelRequire = true;
  newRequire.Module = Module;
  newRequire.modules = modules;
  newRequire.cache = cache;
  newRequire.parent = previousRequire;

  for (var i = 0; i < entry.length; i++) {
    newRequire(entry[i]);
  }

  // Override the current require with this new one
  return newRequire;
})({6:[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
var date = function date() {
    var date = new Date();

    // Jour
    var tab_jour = new Array("Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi");
    var jour = tab_jour[date.getDay()];

    //Jour Nombre
    var jour_nb = date.getDate();

    //Mois
    var tab_mois = new Array("Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre");
    var mois = tab_mois[date.getMonth()];
    return '<p class="sectionTask__header--container--left--jour">' + '<span class="bold">' + jour + ',</span> ' + jour_nb + '</p>' + '<p class="sectionTask__header--container--left--mois">' + mois + '</p>';
};

exports.default = date;
},{}],10:[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
var crudfulConfig = {
  headers: {
    cfAccessKey: "51c4ca332ad21ba1110edadc29ba06c159fce67f"
  }
};

// Permets de trouver les tâches 

var getTasks = exports.getTasks = function getTasks(listId) {
  return axios.get("https://todo.crudful.com/tasks?listId=" + listId, crudfulConfig).then(function (result) {
    return result.data.results;
  });
};

var setTaskIsCompleted = exports.setTaskIsCompleted = function setTaskIsCompleted(taskId, isCompleted) {
  return axios.patch("https://todo.crudful.com/tasks/" + taskId, {
    isCompleted: isCompleted
  }, crudfulConfig).then(function (result) {
    return result.data;
  });
};

var deleteTask = exports.deleteTask = function deleteTask(taskId) {
  return axios.delete("https://todo.crudful.com/tasks/" + taskId, crudfulConfig);
};

var postTask = exports.postTask = function postTask(title, detail, due, listId) {
  return axios.post("https://todo.crudful.com/tasks", { title: title, details: detail, due: due, listId: listId }, crudfulConfig).then(function (result) {
    return result.data;
  });
};

var getLists = exports.getLists = function getLists() {
  return axios.get("https://todo.crudful.com/lists", crudfulConfig).then(function (result) {
    return result.data.results;
  });
};

var deleteList = exports.deleteList = function deleteList(listId) {
  return axios.delete("https://todo.crudful.com/lists/" + listId, crudfulConfig);
};

var postList = exports.postList = function postList(title, color, taskCount) {
  return axios.post("https://todo.crudful.com/lists", { title: title, color: color, taskCount: taskCount }, crudfulConfig).then(function (result) {
    return result.data;
  });
};
},{}],8:[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.refreshAllTasks = undefined;

var _api = require("./api.js");

var ourTasks = [];
var ourListId = "";

var showPanel = function showPanel(panelId) {
  // Hide all panels
  var panels = document.getElementsByClassName("panel");
  for (var i = 0; i < panels.length; i++) {
    panels[i].setAttribute("hidden", "true");
  }
  // Show the panel with panelId
  document.getElementById(panelId).removeAttribute("hidden");
  if (panelId === "tasks-loading" || panelId === "tasks-new") {
    document.getElementById("task-new-link").setAttribute("hidden", "true");
  } else {
    document.getElementById("task-new-link").removeAttribute("hidden");
  }
};

var setTaskCompletion = function setTaskCompletion(taskId, isChecked) {
  (0, _api.setTaskIsCompleted)(taskId, isChecked).then(function (newTaskState) {
    // Replace task in ourTasks by its new state
    for (var i = 0; i < ourTasks.length; i++) {
      if (ourTasks[i].id === taskId) {
        ourTasks[i] = newTaskState;
        break;
      }
    }
    buildList(ourTasks);
  }).catch(function (err) {
    console.error("Something happened when setting task completion", err);
    alert("Une erreur est survenue côté serveur");
    buildList(ourTasks);
  });
};

var deleteButtonClicked = function deleteButtonClicked(taskId) {
  (0, _api.deleteTask)(taskId).then(function () {
    // Delete task from ourTasks
    ourTasks = ourTasks.filter(function (task) {
      return task.id !== taskId;
    });
    buildList(ourTasks);
  }).catch(function (err) {
    console.error("Something happened when deleting a task", err);
    alert("Une erreur est survenue côté serveur");
  });
};

var createTask = function createTask(task, ul) {
  var li = document.createElement("li");
  var p = document.createElement("p");
  var date = document.createElement("p");
  var nb_task = document.getElementById("nbtasks");
  p.innerHTML = "<span class='bold'>Détails : </span>" + task.details;
  var dateFormat = new Date(task.due);
  var todayDate = new Date();
  var options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
  var dateFinal = dateFormat.toLocaleDateString('fr-FR', options);
  date.innerHTML = "<span class='bold'>Date de rendu : </span>" + dateFinal;
  if (dateFormat < todayDate) {
    li.style.background = "background: rgb(204,31,31)";
    li.style.background = "linear-gradient(90deg, rgba(204,31,31,1) 10%, rgba(255,255,255,1) 10%)";
  }
  li.className = "task-li";
  p.className = "task-p";
  date.className = "task-date";
  var checkbox = document.createElement("input");
  checkbox.id = "checkbox_" + task.id;
  checkbox.type = "checkbox";
  checkbox.checked = task.isCompleted;
  checkbox.addEventListener("change", function (evt) {
    return setTaskCompletion(task.id, evt.target.checked);
  });
  li.appendChild(checkbox);
  var title = document.createElement("label");
  title.setAttribute("for", "checkbox_" + task.id);
  title.innerText = task.title;
  title.className = "task-label";
  if (task.isCompleted) {
    title.className = "striked";
  }
  li.appendChild(title);
  var deleteButton = document.createElement("a");
  deleteButton.setAttribute("uk-icon", "trash");
  deleteButton.addEventListener("click", function () {
    return deleteButtonClicked(task.id);
  });
  var count_task = document.getElementsByClassName("task-li").length + 1;
  li.appendChild(deleteButton);
  ul.appendChild(li);
  li.appendChild(p);
  p.appendChild(date);
  nb_task.innerHTML = "<span class='bold'>" + count_task + "</span> tâches";
};

var buildList = function buildList(tasks) {
  if (tasks.length === 0) {
    showPanel("tasks-empty");
  } else {
    // Build the list
    var ul = document.getElementById("tasks-ul");
    ul.innerText = "";
    tasks.forEach(function (task) {
      return createTask(task, ul);
    });
    showPanel("tasks-list");
  }
};

var addNewTask = function addNewTask() {
  var title = document.getElementById("task-new-title").value;
  var details = document.getElementById("task-new-detail").value;
  var due = document.getElementById("task-new-due").value;
  // Create task
  (0, _api.postTask)(title, details, due, ourListId).then(function (task) {
    // Update ourTasks
    ourTasks.push(task);
    buildList(ourTasks);
    showPanel("tasks-list");
    document.getElementById("task-new-title").value = "";
    document.getElementById("task-new-detail").value = "";
    document.getElementById("task-new-due").value = "";
  }).catch(function (err) {
    console.error("Could not create task", err);
    alert("Une erreur est survenue côté serveur");
  });
};

var refreshAllTasks = exports.refreshAllTasks = function refreshAllTasks(listId) {
  showPanel("tasks-loading");
  ourListId = listId;
  (0, _api.getTasks)(listId).then(function (tasks) {
    ourTasks = tasks;
    buildList(tasks);
  });
};

var initTasks = function initTasks() {
  showPanel("tasks-loading");
  document.getElementById("task-new-link").addEventListener("click", function () {
    return showPanel("tasks-new");
  });
  document.getElementById("task-new-button").addEventListener("click", addNewTask);
  document.getElementById("task-new-cancel").addEventListener("click", function () {
    return showPanel("tasks-list");
  });
};

exports.default = initTasks;
},{"./api.js":10}],7:[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.refreshAllLists = undefined;

var _api = require("./api.js");

var ourLists = [];

var showPanel = function showPanel(panelId) {
  // Hide all panels
  var panels = document.getElementsByClassName("panel");
  for (var i = 0; i < panels.length; i++) {
    panels[i].setAttribute("hidden", "true");
  }
  // Show the panel with panelId
  document.getElementById(panelId).removeAttribute("hidden");
  if (panelId === "lists-loading" || panelId === "lists-new") {
    document.getElementById("list-new-link").setAttribute("hidden", "true");
  } else {
    document.getElementById("list-new-link").removeAttribute("hidden");
  }
};

var deleteButtonClicked = function deleteButtonClicked(listId) {
  (0, _api.deleteList)(listId).then(function () {
    // Delete list from ourLists
    ourLists = ourLists.filter(function (list) {
      return list.id !== listId;
    });
    buildList(ourLists);
  }).catch(function (err) {
    console.error("Something happened when deleting a list", err);
    alert("Une erreur est survenue côté serveur");
  });
};

var createList = function createList(list, ul) {
  var li = document.createElement("li");
  li.className = "list-li";
  var link = document.createElement("a");
  li.style.background = "#" + list.color;
  li.style.background = "linear-gradient(90deg, #" + list.color + " 20%, #fbfbff 20%)";
  link.innerText = list.title;
  link.href = "#tasks/" + list.id;
  link.className = "list-tasks-link";
  li.appendChild(link);
  var deleteButton = document.createElement("a");
  deleteButton.setAttribute("uk-icon", "trash");
  deleteButton.className = "icon-trash";
  deleteButton.addEventListener("click", function () {
    return deleteButtonClicked(list.id);
  });
  li.appendChild(deleteButton);
  ul.appendChild(li);
};

var buildList = function buildList(lists) {
  if (lists.length === 0) {
    showPanel("lists-empty");
  } else {
    // Build the list
    var ul = document.getElementById("lists-ul");
    ul.innerText = "";
    lists.forEach(function (list) {
      return createList(list, ul);
    });
    showPanel("lists-list");
  }
};

var addNewList = function addNewList() {
  var title = document.getElementById("list-new-title").value;
  var colorValue = document.getElementById("color_list").value;
  console.log(colorValue);
  var color = colorValue.replace("#", "");
  console.log(color);

  // Create task
  (0, _api.postList)(title, color).then(function (list) {
    // Update ourLists
    ourLists.push(list);
    buildList(ourLists);
    showPanel("lists-list");
    document.getElementById("list-new-title").value = "";
    document.getElementById("color_list").value = "";
  }).catch(function (err) {
    console.error("Could not create list", err);
    alert("Une erreur est survenue côté serveur");
  });
};

var refreshAllLists = exports.refreshAllLists = function refreshAllLists() {
  showPanel("lists-loading");
  (0, _api.getLists)().then(function (lists) {
    ourLists = lists;
    buildList(lists);
  });
};

var initLists = function initLists() {
  document.getElementById("list-new-link").addEventListener("click", function () {
    return showPanel("lists-new");
  });
  document.getElementById("list-new-button").addEventListener("click", addNewList);
  document.getElementById("list-new-cancel").addEventListener("click", function () {
    return showPanel("lists-list");
  });
};

exports.default = initLists;
},{"./api.js":10}],3:[function(require,module,exports) {
"use strict";

var _date = require("./components/date.js");

var _date2 = _interopRequireDefault(_date);

var _tasks = require("./components/tasks.js");

var _tasks2 = _interopRequireDefault(_tasks);

var _lists = require("./components/lists.js");

var _lists2 = _interopRequireDefault(_lists);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// We'll handle the SPA here
var showPage = function showPage(pageId) {
  var pages = document.getElementsByClassName("page");
  for (var i = 0; i < pages.length; i++) {
    var page = pages[i];
    if (page.id === pageId) {
      page.removeAttribute("hidden");
    } else {
      page.setAttribute("hidden", "true");
    }
  }
};

var selectPage = function selectPage() {
  var hash = document.location.hash.replace("#", "");
  var path = hash.split("/");
  var page = path[0];
  switch (page) {
    case "tasks":
      showPage("tasks-page");
      document.getElementById("task-new-link").removeAttribute("hidden");
      document.getElementById("nbtasks").removeAttribute("hidden");
      (0, _tasks.refreshAllTasks)(path[1]);
      break;
    default:
      showPage("lists-page");
      document.getElementById("task-new-link").setAttribute("hidden", "true");
      document.getElementById("nbtasks").innerHTML = "";
      (0, _lists.refreshAllLists)();
      break;
  }
};

var init = function init() {
  (0, _lists2.default)();
  (0, _tasks2.default)();
  selectPage();
};

window.addEventListener("load", init);
window.addEventListener("hashchange", selectPage);
document.getElementById("dateDuJour").innerHTML = (0, _date2.default)();
},{"./components/date.js":6,"./components/tasks.js":8,"./components/lists.js":7}],11:[function(require,module,exports) {

var global = (1, eval)('this');
var OldModule = module.bundle.Module;
function Module(moduleName) {
  OldModule.call(this, moduleName);
  this.hot = {
    accept: function (fn) {
      this._acceptCallback = fn || function () {};
    },
    dispose: function (fn) {
      this._disposeCallback = fn;
    }
  };
}

module.bundle.Module = Module;

var parent = module.bundle.parent;
if ((!parent || !parent.isParcelRequire) && typeof WebSocket !== 'undefined') {
  var hostname = '' || location.hostname;
  var protocol = location.protocol === 'https:' ? 'wss' : 'ws';
  var ws = new WebSocket(protocol + '://' + hostname + ':' + '60031' + '/');
  ws.onmessage = function (event) {
    var data = JSON.parse(event.data);

    if (data.type === 'update') {
      data.assets.forEach(function (asset) {
        hmrApply(global.require, asset);
      });

      data.assets.forEach(function (asset) {
        if (!asset.isNew) {
          hmrAccept(global.require, asset.id);
        }
      });
    }

    if (data.type === 'reload') {
      ws.close();
      ws.onclose = function () {
        location.reload();
      };
    }

    if (data.type === 'error-resolved') {
      console.log('[parcel] ✨ Error resolved');
    }

    if (data.type === 'error') {
      console.error('[parcel] 🚨  ' + data.error.message + '\n' + 'data.error.stack');
    }
  };
}

function getParents(bundle, id) {
  var modules = bundle.modules;
  if (!modules) {
    return [];
  }

  var parents = [];
  var k, d, dep;

  for (k in modules) {
    for (d in modules[k][1]) {
      dep = modules[k][1][d];
      if (dep === id || Array.isArray(dep) && dep[dep.length - 1] === id) {
        parents.push(+k);
      }
    }
  }

  if (bundle.parent) {
    parents = parents.concat(getParents(bundle.parent, id));
  }

  return parents;
}

function hmrApply(bundle, asset) {
  var modules = bundle.modules;
  if (!modules) {
    return;
  }

  if (modules[asset.id] || !bundle.parent) {
    var fn = new Function('require', 'module', 'exports', asset.generated.js);
    asset.isNew = !modules[asset.id];
    modules[asset.id] = [fn, asset.deps];
  } else if (bundle.parent) {
    hmrApply(bundle.parent, asset);
  }
}

function hmrAccept(bundle, id) {
  var modules = bundle.modules;
  if (!modules) {
    return;
  }

  if (!modules[id] && bundle.parent) {
    return hmrAccept(bundle.parent, id);
  }

  var cached = bundle.cache[id];
  if (cached && cached.hot._disposeCallback) {
    cached.hot._disposeCallback();
  }

  delete bundle.cache[id];
  bundle(id);

  cached = bundle.cache[id];
  if (cached && cached.hot && cached.hot._acceptCallback) {
    cached.hot._acceptCallback();
    return true;
  }

  return getParents(global.require, id).some(function (id) {
    return hmrAccept(global.require, id);
  });
}
},{}]},{},[11,3])
//# sourceMappingURL=/dist/924156c6845c924cc36f428cc48b2352.map