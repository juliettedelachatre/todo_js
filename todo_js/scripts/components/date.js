const date = () => {
    let date = new Date()

    // Jour
    let tab_jour=new Array("Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi");
    let jour = tab_jour[date.getDay()];

    //Jour Nombre
    let jour_nb = date.getDate();

    //Mois
    let tab_mois=new Array("Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre");
    let mois = tab_mois[date.getMonth()];
    return '<p class="sectionTask__header--container--left--jour">' +
               '<span class="bold">' + jour + ',</span> ' +
               jour_nb +
            '</p>' +
            '<p class="sectionTask__header--container--left--mois">' + mois + '</p>';
}

export default date;