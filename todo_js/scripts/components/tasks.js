import {
    deleteTask,
    getTasks,
    postTask,
    setTaskIsCompleted,
  } from "./api.js";
  
  let ourTasks = [];
  let ourListId = "";
  
  const showPanel = (panelId) => {
    // Hide all panels
    const panels = document.getElementsByClassName("panel");
    for (let i = 0; i < panels.length; i++) {
      panels[i].setAttribute("hidden", "true");
    }
    // Show the panel with panelId
    document.getElementById(panelId).removeAttribute("hidden");
    if (panelId === "tasks-loading" || panelId === "tasks-new") {
      document
        .getElementById("task-new-link")
        .setAttribute("hidden", "true");
    } else {
      document
        .getElementById("task-new-link")
        .removeAttribute("hidden");
    }
  };
  
  const setTaskCompletion = (taskId, isChecked) => {
    setTaskIsCompleted(taskId, isChecked)
      .then((newTaskState) => {
        // Replace task in ourTasks by its new state
        for (let i = 0; i < ourTasks.length; i++) {
          if (ourTasks[i].id === taskId) {
            ourTasks[i] = newTaskState;
            break;
          }
        }
        buildList(ourTasks);
      })
      .catch((err) => {
        console.error(
          "Something happened when setting task completion",
          err
        );
        alert("Une erreur est survenue côté serveur");
        buildList(ourTasks);
      });
  };
  
  const deleteButtonClicked = (taskId) => {
    deleteTask(taskId)
      .then(() => {
        // Delete task from ourTasks
        ourTasks = ourTasks.filter((task) => task.id !== taskId);
        buildList(ourTasks);
      })
      .catch((err) => {
        console.error("Something happened when deleting a task", err);
        alert("Une erreur est survenue côté serveur");
      });
  };
  
  const createTask = (task, ul) => {
    const li = document.createElement("li");
    const p = document.createElement("p");
    const date = document.createElement("p");
    const nb_task = document.getElementById("nbtasks");
    p.innerHTML = "<span class='bold'>Détails : </span>" + task.details;
    const dateFormat = new Date(task.due);
    const todayDate = new Date();
    const options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
    const dateFinal = dateFormat.toLocaleDateString('fr-FR', options);
    date.innerHTML = "<span class='bold'>Date de rendu : </span>" + dateFinal;
    if(dateFormat < todayDate) {
        li.style.background = "background: rgb(204,31,31)";
        li.style.background = "linear-gradient(90deg, rgba(204,31,31,1) 10%, rgba(255,255,255,1) 10%)";
    }
    li.className = "task-li";
    p.className = "task-p";
    date.className = "task-date";
    const checkbox = document.createElement("input");
    checkbox.id = `checkbox_${task.id}`;
    checkbox.type = "checkbox";
    checkbox.checked = task.isCompleted;
    checkbox.addEventListener("change", (evt) =>
      setTaskCompletion(task.id, evt.target.checked)
    );
    li.appendChild(checkbox);
    const title = document.createElement("label");
    title.setAttribute("for", `checkbox_${task.id}`);
    title.innerText = task.title;
    title.className = "task-label";
    if (task.isCompleted) {
      title.className = "striked";
    }
    li.appendChild(title);
    const deleteButton = document.createElement("a");
    deleteButton.setAttribute("uk-icon", "trash");
    deleteButton.addEventListener("click", () =>
      deleteButtonClicked(task.id)
    );
    const count_task = document.getElementsByClassName("task-li").length+1;
    li.appendChild(deleteButton);
    ul.appendChild(li);
    li.appendChild(p);
    p.appendChild(date);
    nb_task.innerHTML = "<span class='bold'>" + count_task + "</span> tâches";
  };
  
  const buildList = (tasks) => {
    if (tasks.length === 0) {
      showPanel("tasks-empty");
    } else {
      // Build the list
      const ul = document.getElementById("tasks-ul");
      ul.innerText = "";
      tasks.forEach((task) => createTask(task, ul));
      showPanel("tasks-list");
    }
  };
  
  const addNewTask = () => {
    const title = document.getElementById("task-new-title").value;
    const details = document.getElementById("task-new-detail").value;
    const due = document.getElementById("task-new-due").value;
    // Create task
    postTask(title, details, due, ourListId)
      .then((task) => {
        // Update ourTasks
        ourTasks.push(task);
        buildList(ourTasks);
        showPanel("tasks-list");
        document.getElementById("task-new-title").value = "";
        document.getElementById("task-new-detail").value = "";
        document.getElementById("task-new-due").value = "";
      })
      .catch((err) => {
        console.error("Could not create task", err);
        alert("Une erreur est survenue côté serveur");
      });
  };
  
  export const refreshAllTasks = (listId) => {
    showPanel("tasks-loading");
    ourListId = listId;
    getTasks(listId).then((tasks) => {
      ourTasks = tasks;
      buildList(tasks);
    });
  };
  
  const initTasks = () => {
    showPanel("tasks-loading");
    document
      .getElementById("task-new-link")
      .addEventListener("click", () => showPanel("tasks-new"));
    document
      .getElementById("task-new-button")
      .addEventListener("click", addNewTask);
    document
      .getElementById("task-new-cancel")
      .addEventListener("click", () => showPanel("tasks-list"));
  };
  
  export default initTasks;
  