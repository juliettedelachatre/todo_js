const crudfulConfig = {
    headers: {
      cfAccessKey: "51c4ca332ad21ba1110edadc29ba06c159fce67f",
    },
  };
  

  // Permets de trouver les tâches 

  export const getTasks = (listId) =>
  axios
    .get(
      `https://todo.crudful.com/tasks?listId=${listId}`,
      crudfulConfig
    )
    .then((result) => result.data.results);

export const setTaskIsCompleted = (taskId, isCompleted) =>
  axios
    .patch(
      `https://todo.crudful.com/tasks/${taskId}`,
      {
        isCompleted: isCompleted,
      },
      crudfulConfig
    )
    .then((result) => result.data);

export const deleteTask = (taskId) =>
  axios.delete(
    `https://todo.crudful.com/tasks/${taskId}`,
    crudfulConfig
  );

export const postTask = (title, detail, due, listId) =>
  axios
    .post(
      "https://todo.crudful.com/tasks",
      { title: title, details: detail, due: due, listId: listId, },
      crudfulConfig
    )
    .then((result) => result.data);

export const getLists = () =>
  axios
    .get("https://todo.crudful.com/lists", crudfulConfig)
    .then((result) => result.data.results);

export const deleteList = (listId) =>
  axios.delete(
    `https://todo.crudful.com/lists/${listId}`,
    crudfulConfig
  );

export const postList = (title, color, taskCount) =>
  axios
    .post(
      "https://todo.crudful.com/lists",
      { title: title, color: color, taskCount: taskCount },
      crudfulConfig
    )
    .then((result) => result.data);
