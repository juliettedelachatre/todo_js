import date from "./components/date.js";
import initTasks, { refreshAllTasks} from "./components/tasks.js";
import initLists, { refreshAllLists } from "./components/lists.js";

// We'll handle the SPA here
const showPage = (pageId) => {
    const pages = document.getElementsByClassName("page");
    for (let i = 0; i < pages.length; i++) {
      const page = pages[i];
      if (page.id === pageId) {
        page.removeAttribute("hidden");
      } else {
        page.setAttribute("hidden", "true");
      }
    }
  };

  const selectPage = () => {
    const hash = document.location.hash.replace("#", "");
    const path = hash.split("/");
    const page = path[0];
    switch (page) {
      case "tasks":
        showPage("tasks-page");
        document
            .getElementById("task-new-link")
            .removeAttribute("hidden")  
        document
            .getElementById("nbtasks")
            .removeAttribute("hidden")         
        refreshAllTasks(path[1]);
        break;
      default:
        showPage("lists-page");
        document
            .getElementById("task-new-link")
            .setAttribute("hidden", "true")
        document.getElementById("nbtasks").innerHTML = "";
        refreshAllLists();
        break;
    }
  };

  const init = () => {
    initLists();
    initTasks();
    selectPage();
  };


window.addEventListener("load", init);
window.addEventListener("hashchange", selectPage);
document.getElementById("dateDuJour").innerHTML = date();
